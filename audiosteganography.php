<?php

  /*
   * 
   *  These classes convert any jpg, png, or gif into a 44.1khz 16bit single channel wav file
   *  Written By: Travis Hyyppa
   *  Date: 2011-04
   *  
   *  Note: The larger the image, the longer the script will run.
   *        At Default settings an image takes about 2 minutes.
   *        It is strongly recommended that you do not run this on a production server.
   * 
   */

  class audioSteganography{

    private $samplerate,
            $bits, 
            $samples,
            $error;
    
    public  $max_height,
            $max_width,
            $low_freq,
            $high_freq,
            $ppsec,
            $web_width,
            $info;
    
    const TAU = 6.283185307179586;
    
    /* 
     * 
     * Construct Params:
     *                  mwid = max width of incoming image
     *                  mhei = max height of incoming imae
     *
     */
    public function __construct($mwid = 100, $mhei = 100){
      set_time_limit(0);            # This script takes a few minutes to run, we don't want it timing out.
      $this->samplerate = 44100;    # 44.1khz is a standard wave samplerate
      $this->bits       = 16;       # Two bytes per sample - 16bits
      $this->max_height = $mwid;    # Max width of the incoming image defaults to 100px - scale down if over this
      $this->max_width  = $mhei;    # Max height default is 100px
      $this->low_freq   = 200;      # Frequency of the "lowest" pixels (highest y value)
      $this->high_freq  = 20000;    # Frequency of the "highest" pixels (y value of 0)      
      $this->ppsec      = 10;       # Pixels rendered Per Second of audio
      $this->web_width  = 600;      # The max width of the image saved for web viewing.
      
      $this->info['samplerate'] = $this->samplerate;
      $this->info['bits']       = $this->bits;
      $this->info['ppsec']      = $this->ppsec;
      $this->info['freqrange']  = $this->high_freq - $this->low_freq;
      
      # Be sure the GD image library is enabled
      try{
        gd_info();
      }catch(Exception $e){
        throw new Exception('GD Image library needs to be installed and enabled!');
      }
    }
    
    /*
     * 
     * Takes an array of audio data, packs it into a 16bit format, and adds the correct wave header
     * Returns wave data, does not write the file
     * 
     */
    private function array2wav($audioData){
      
      # Count total samples in audio for header data
      $this->samples = count($audioData) + 1;
      
      # Add total samples to info array
      $this->info['samples'] = $this->samples;      
      
      # Loop through each sample and pack it into a data string with 2bytes per sample
      for($n=0;$n < ($this->samples);$n++){
        $data .= pack('v',$audioData[$n]);
      }
      
      # Create the header and append the data string
      return $this->create_header() . $data;
    }
    
    /*
     * 
     * This is the main method of the class, inputs a file, an output file
     * and a boolean for echoing progress to a file
     * 
     * 
     */
    public function img2audio($inFile,$outFile,$echoProgress){
      
      # Create a new exception handler that will save errors to disk
      $this->error = new stegExceptionHandler($outFile);
      
      # Over-ride the existing error handler
      set_exception_handler(array($this->error,'exception_handler'));
      
      # Get the time at which the script starts the long process of rendering
      $startTime = $this->getTime();
      
      # Create a gd image from inFile - jpg, png, or gif
      $img = $this->createImageFromFile($inFile);
      
      # Get the width and height of the input image
      list($width,$height) = getimagesize($inFile);
      
      # Scale the image down (this method also saves three scales to output directory)
      $this->scale($img,$width,$height,$outFile);
      
      # Get the frequency interval: difference in hz per y values;
      $freqrange = $this->high_freq-$this->low_freq;
      $interval = $freqrange / $height;
      
      # Calculate samples per x pixel;
      $samplespixel =  floor($this->samplerate / $this->ppsec);
      
      # Determine how many samples audio file will require
      $samples = $samplespixel * $width;

      # Set current calculation to 0 - used for tracking progress
      $current_calc = 0;
      
      # Determine how many calculations will be performed - used for tracking progress
      $total_calcs = ($width+1)*($height+1)*$samplespixel;
      
      # Loop through the image on the x axis
      for($x=0; $x <= $width; $x++){
        # Keep rendering the wave for a bit
        for($i=0;$i < $samplespixel; $i++){
          # Clean the sine buffer
          $sine = 0;
          # Loop through the y axis of the image
          for($y=0; $y <= $height; $y++){
            # Get the frequency of this y value
            $freq = $y*$interval;
            # Add this frequency using i as time to the sine buffer at the amplitude
            # returned by getIntensity for the pixel at x,y
            $cycles = $samples * $freq / $this->samplerate;
            $sine += (sin($cycles * self::TAU * $i / $samples) * $this->getIntensity(imagecolorat($img,$x,$height-$y)));
            
            # if progress echoing is enabled, save a file (audio.wav.prg) containing the percentage complete
            # Todo: Find a way to not use the HDD for this - will not scale well.  Maybe TCPIP sockets or output buffer
            if($echoProgress && !($current_calc % 200)){
              # Feed the trackProgress function..
              $this->trackProgress($current_calc, $total_calcs, $outFile);
            }
            $current_calc++;
          }
          # Add the sinewave containing all frequencies for this x value to output array
          $audioOut[] = $sine;  
        }
      }
      
      # After all of the data has been created, use array2wav to pack it up and add a header
      $wavData = $this->array2wav($audioOut);
      
      # Then save the file
      try{
        $file = fopen($outFile, 'w');
        fwrite($file, $wavData);
        fclose($file);
      }catch(Exception $e){
        throw new Exception('Error writing audio to disk.');
      }
      
      # Time of script's completion
      $endTime = $this->getTime();
      
      # Add execution time to info array
      $this->info['execution_time'] = $endTime - $startTime;
      
      return;
    }    
    
    /*
     * 
     *  This method inputs an RGB color and outputs a proper amplitude for the audio
     * 
     */
    private function getIntensity($color){
      
      // imagecolorat() returns an integer in 24bit RRRRRRRRGGGGGGGGBBBBBBBB format, we need to break that up.
      $r = ($color >> 16) & 0xFF;   // Bitshift 16 to the left and mask off the first 8 bits
      $g = ($color >> 8) & 0xFF;    // Bitshift 8 and mask off first 8 bits
      $b = $color & 0xFF;           // Just mask off the first 8 bits for blue
      
      // To get a nice grayscale image we're using 30% R , 59% G, 11% B
      $lumin = ((0.30 * $r) + (0.59 * $g) + (0.11 * $b)) * 0.25;
      
      return $lumin;
    }
    
    /*
     * 
     * Input current_calculation, total calculations and output percentage to file
     * 
     */
    private function trackProgress($current,$total,$outFile){
      try{
        $percent = round(($current/$total)*100,2);
        file_put_contents($outFile.'.prg',$percent);
        return;
      }catch(Exception $e){
        throw new Exception('Error writing progress to disk.');
      }
    }
    
    
    /*
     * 
     * Returns the time in seconds discarding ms
     * Used to track script execution time.
     * 
     */
    private function getTime(){
        list($msec, $sec) = explode(' ', microtime());
        return $sec;
    }
    
    
    /*
     * 
     *  Useful little function that creates a gd image from jpg,png,and gif files (animated gifs okay, will use frame 1)
     * 
     */
    private function createImageFromFile($file){
      
        // Try as a jpeg, if failed returns 0 and does not return from this method
        $img = @imagecreatefromjpeg($file);
        if($img){
            $this->info['filetype'] = "JPEG";
            return $img;
        }
        
        // Try as png
        $img = @imagecreatefrompng($file);
        if($img){
            $this->info['filetype'] = "PNG";
            return $img;
        }
        
        // Try as gif
        $img = @imagecreatefromgif($file);
        if($img){
            $this->info['filetype'] = "GIF";
            return $img;
        }
        
        // NOTE: file extensions are useless in this case as all files are saved as .TMPs
        
        if(!$img){
          throw new Exception('Incompatible image format! Please upload a JPEG, PNG, or GIF');
        }
        
        return;
    }
    
    /*
     * 
     * Scales image down so that the script doesn't take hours to complete.
     * Also saves a few different sizes of the image for use on webpage.
     * TODO: Clean up this method.
     * 
     */
    private function scale(&$img,&$width,&$height,$outfile){
      
      # Keep track of the original dimensions
      $old_width = $width;
      $old_height= $height;
      
      if($width < $this->web_width)
              $this->web_width = $width;
      
      if($width < 200){
          $this->web_width = 200;
      }
      
      # Just grabbing some info
      $this->info['inwid'] = $width;
      $this->info['inhei'] = $height;
      
      # If the image is too wide..
      if($width > $this->max_width){        
        # Set the new height and width
        $width = $this->max_width;
        $height = floor($old_height * ($width / $old_width));
        
      # If it's too tall
      }elseif($height > $this->max_height){
        # Set new height and width
        $height = $this->max_height;
        $width = floor($old_width * ($height / $old_height));
      }
      
      try{
        # Save copy of original
        imagejpeg($img,$outfile.'_orig.jpg');

        # Scale down and save a web-sized version
        $percentage = $this->web_width/$old_width;
        $halfimg = imagecreatetruecolor(ceil($old_width*$percentage),ceil($old_height*$percentage));
        imagecopyresampled($halfimg, $img, 0, 0, 0, 0, ceil($old_width*$percentage), ceil($old_height*$percentage), $old_width, $old_height);
        imagejpeg($halfimg,$outfile.'_half.jpg');
        unset($halfimg);

        # Create the final scaled image for use in the img2wav method
        $newimg = imagecreatetruecolor($width,$height);
        imagecopyresampled($newimg, $img, 0, 0, 0, 0, $width, $height, $old_width, $old_height);
        $img = $newimg;  # $img is a pointer
        imagejpeg($img,$outfile.'.jpg');
        
        # Final dimensions after scaling -> info array
        $this->info['outwid'] = $width;
        $this->info['outhei'] = $height;
        
      }catch(Exception $e){
        throw new Exception('Could not create images on disk!');
      }
      
      return;
    }
    
    /*
     * 
     * This method creates a wav file header
     * Because this script only creates 44.1khz 16bit single channel wavs
     * it is a very simple function
     * 
     */
    private function create_header(){
        $byterate   = $this->samplerate * $this->bits / 8;
        $blockalign = $this->bits / 8;
        $filesize   = $this->samples * ($this->bits/8) + 36; # 36 = the length of this header

        // Write file header
        $output .= pack('a4Va4','RIFF',$filesize,'WAVE');
        $output .= pack('a4VvvVVvv',
                          'fmt ',16,1,1,
                                $this->samplerate,
                                $byterate,
                                $blockalign,
                                $this->bits);
  		
        $output .= pack('a4V','data',$blockalign * $this->samples);

        return $output;
    }
    
    
  }
  
  /*
   * 
   * This little helper class simply handles any errors and outputs them to disk.
   * 
   */
  class stegExceptionHandler{
    
      private $outFile;

      public function __construct($outFile) {
          $this->outFile = $outFile . '.error';
      }

      public function exception_handler($e){
          $message = $e->getMessage(). "\n";
          $message .= 'Line: ' . $e->getLine();
          $this->writeToFile($message);
      }

      private function writeToFile($message){
        $file = fopen($this->outFile, 'w');
        fwrite($file, $message);
        fclose($file);
        return;
      }

  }